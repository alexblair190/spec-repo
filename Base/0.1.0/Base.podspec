Pod::Spec.new do |s|
  s.name             = 'Base'
  s.version          = '0.1.0'
  s.summary          = 'Base is UI construction framework.'
  s.description      = <<-DESC
                       Base is a UI framework created with conventions and convenience in mind.
                       Base ensures consistency around UI construction throughout the entire application, making PRs cleaner,
                       and errors easier to spot. This framework is intended to be used with MVVM with each BaseView owning its
                       own model and function for populating the interface. Questions regarding whether or not populate(using: Model)
                       should be part of the construction protocol are still in debate as the framework is still being tweaked.

                       Models should be dumb and should only include foundational types. If custom data types need to be used,
                       an adapter should be created for the model where any business logic can reside. View model adapters are easy to test and should
                       always be tested. This is an easy way to get coverage around UI without a robust testing suite. These tests are unit tests
                       and are not responsible for validating UI components.
                       DESC
  s.homepage         = 'https://bitbucket.org/alexblair190/base/src/master/'
  # s.screenshots    = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license      = { :type => 'MIT', :text => <<-LICENSE
                        Copyright (c) 2019 alexblair1 <alexblair190@gmail.com>
                     LICENSE
                   }
  s.author           = { 'Alex Blair' => 'alexblair190@gmail.com' }
  s.source           = { :git => 'https://alexblair190@bitbucket.org/alexblair190/base.git', :tag => s.version }
  s.swift_version    = '4.2'

  s.ios.deployment_target = '8.0'

  s.source_files = 'Base/Classes/**/*'
  
  # s.resource_bundles = {
  #   'Base' => ['Base/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
